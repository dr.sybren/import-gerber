# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Gerber PCB import",
    "author": "dr. Sybren A. Stüvel",
    "version": (0, 1),
    "blender": (2, 80, 0),
    "location": "File > Import",
    "description": "Import PCB definitions in Gerber files",
    "warning": "",
    "wiki_url": "",
    "category": "Import-Export"
}

if 'bpy' in locals():
    import importlib
    importer = importlib.reload(importer)
else:
    from . import importer

import pathlib
import bpy

from bpy.props import StringProperty, FloatProperty
from bpy_extras.io_utils import ImportHelper


class CURVE_OT_import_gerber(bpy.types.Operator, ImportHelper):
    """Load a Gerber ZIP file"""
    bl_idname = "curve.import_gerber"
    bl_label = "Import Gerber"
    bl_options = {'REGISTER', 'UNDO'}

    filename_ext = ".zip"
    filter_glob = StringProperty(default="*.zip", options={'HIDDEN'})

    global_scale = FloatProperty(
            name="Scale",
            description="Scale the file by this value",
            min=0.0001, max=1000000.0,
            soft_min=0.001, soft_max=100.0,
            default=1.0,
            )

    def execute(self, context):
        filepath = pathlib.Path(self.filepath)
        if not filepath.exists():
            self.report({'ERROR'}, 'File %s does not exist' % self.filepath)
            return {'CANCELLED'}
        if filepath.is_dir():
            self.report({'ERROR'}, 'Path %s is a directory, not a file' % self.filepath)
            return {'CANCELLED'}

        ok = importer.load(context, report=self.report,
                           filepath=filepath,
                           scale=self.global_scale)
        return {'FINISHED'} if ok else {'CANCELLED'}


def menu_func_import(self, context):
    self.layout.operator(CURVE_OT_import_gerber.bl_idname, text='Gerber (.zip)')


def register():
    bpy.utils.register_class(CURVE_OT_import_gerber)
    bpy.types.INFO_MT_file_import.append(menu_func_import)


def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_import.remove(menu_func_import)


if __name__ == "__main__":
    register()
