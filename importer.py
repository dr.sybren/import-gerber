import logging
import math
import pathlib
import zipfile

if 'bpy' in locals():
    import importlib
    gerber = importlib.reload(gerber)
else:
    import sys

    from .vendor import gerber

    # Workaround for gerber.render automatically importing .cairo_backend
    # See https://github.com/curtacircuitos/pcb-tools/issues/83
    for key, value in sys.modules.items():
        if value is gerber:
            break
    else:
        print('Unable to find just-imported gerber module, things wil break.')
    sys.modules[f'{key}.render.cairo_backend'] = sys.modules[__name__]
    sys.modules[f'{key}.render.cairo_backend'].GerberCairoContext = None
    from .vendor.gerber.render.render import GerberContext

import bpy

ext_to_name = {
    '.GKO': 'Board Outline',
    '.GBL': 'Bottom Layer',
    '.GBO': 'Bottom Silk',
    '.GBS': 'Bottom Solder Mask',
    '.DRL': 'Drill Holes',
    '.GTL': 'Top Layer',
    '.GTO': 'Top Silk',
    '.GTS': 'Top Solder Mask',
}

def load(context, report, filepath: pathlib.Path, scale: float) -> bool:
    """Load a Gerber ZIP file."""

    # Top-level collection for everything in this zip file
    coll = bpy.data.collections.new(filepath.stem)
    context.scene.collection.children.link(coll)

    with zipfile.ZipFile(filepath) as zfile:
        for fname in zfile.namelist():
            fpath = pathlib.Path(fname)
            try:
                coll_name = ext_to_name[fpath.suffix.upper()]
            except KeyError:
                coll_name = fname
            subcoll = bpy.data.collections.new(coll_name)
            coll.children.link(subcoll)

            render_ctx = BlenderGerberContext(context, report, subcoll, scale=scale)

            data = zfile.open(fname, 'r').read()
            as_text = data.decode()
            gfile = gerber.loads(as_text, filename=fname)
            gfile.render(ctx=render_ctx)

    return True


class BlenderGerberContext(GerberContext):
    log = logging.getLogger(f'{__name__}.BlenderGerberContext')

    def __init__(self, context, report, subcoll, scale: float):
        super().__init__()  # TODO(Sybren): figure out how to pass the units
        self.context = context
        self.report = report
        self.subcoll = subcoll
        self.log.info('Creating gerber context for %s', subcoll.name)
        self.data = bpy.data.curves.new(subcoll.name, 'CURVE')
        self.scale = scale

    def set_bounds(self, bounding_box):
        self.log.info('set_bounds(%r)', bounding_box)

    def _paint_background(self):
        pass

    def _new_render_layer(self):
        pass

    def _flatten(self):
        self.log.info('Creating object for %r', self.data)
        ob = bpy.data.objects.new(self.subcoll.name, self.data)
        self.subcoll.objects.link(ob)

    def _scale(self, point) -> (float, float, float, float):
        """Scales from 2D point in mm to 4D point in metres."""

        return (self.scale * point[0] / 1000,
                self.scale * point[1] / 1000,
                0, 0)

    def _disc(self, pos: (float, float), r: float):
        spline = self.data.splines.new('BEZIER')
        spline.bezier_points.add(3)
        x, y = pos
        spline.bezier_points[0].co = self._scale((x,     y - r))[:3]
        spline.bezier_points[1].co = self._scale((x + r, y    ))[:3]
        spline.bezier_points[2].co = self._scale((x,     y + r))[:3]
        spline.bezier_points[3].co = self._scale((x - r, y    ))[:3]

        for bp in spline.bezier_points:
            bp.handle_left_type = 'AUTO'
            bp.handle_right_type = 'AUTO'
        spline.use_cyclic_u = True

        return spline

    def _line(self, vtx1, vtx2):
        spline = self.data.splines.new('POLY')
        spline.points.add(1)
        spline.points[0].co = self._scale(vtx1)
        spline.points[1].co = self._scale(vtx2)

    def _render_line(self, primitive, color):
        # self.log.debug('    line(%r, %r)', primitive, color)
        self._line(primitive.start, primitive.end)

    def _render_arc(self, primitive, color):
        self.log.debug('    arc(%r, %r) sweep_angle=%s', primitive, color, primitive.sweep_angle)
        if abs(primitive.sweep_angle) < 1e-3:
            self._disc(primitive.center, primitive.radius)
            return

        spline = self.data.splines.new('NURBS')

        x1, y1 = primitive.start
        xc, yc = primitive.center
        x4, y4 = primitive.end

        # Source: https://stackoverflow.com/a/44829356/875379
        # NOTE: this only works for counterclockwise arcs
        ax = x1 - xc
        ay = y1 - yc
        bx = x4 - xc
        by = y4 - yc
        div = (ax * by - ay * bx)
        if abs(div) < 1e-6:
            self._line(primitive.start, primitive.end)
            return

        q1 = ax * ax + ay * ay
        q2 = q1 + ax * bx + ay * by
        k2 = 4/3 * ((2 * q1 * q2) ** 0.5 - q2) / div

        x2 = xc + ax - k2 * ay
        y2 = yc + ay + k2 * ax
        x3 = xc + bx + k2 * by
        y3 = yc + by - k2 * bx

        spline.points.add(3)
        spline.points[0].co = self._scale(primitive.start)
        spline.points[1].co = self._scale((x2, y2))
        spline.points[2].co = self._scale((x3, y3))
        spline.points[3].co = self._scale(primitive.end)

        spline.order_u = 4  # 4 control points; actually 3rd order curve.
        spline.use_endpoint_u = True
        for p in spline.points:
            p.weight = 1.0

    def _render_region(self, primitive, color):
        self.log.debug('    region(%r, %r)', primitive, color)

    def _render_circle(self, primitive, color):
        # self.log.debug('    circle(%r, %r)', primitive, color)
        self._disc(primitive.position, primitive.radius)

        if primitive.hole_radius:
            self._disc(primitive.position, primitive.hole_radius)

    def _render_rectangle(self, primitive, color):
        # self.log.debug('    rectangle(%r, %r)', primitive, color)
        vertices = primitive.vertices
        for v1, v2 in zip(vertices, vertices[1:] + vertices[:1]):
            self._line(v1, v2)

    def _render_obround(self, primitive, color):
        self.log.debug('    obround(%r, %r)', primitive, color)

    def _render_polygon(self, primitive, color):
        # self.log.debug('    polygon(%r, %r)', primitive, color)
        vertices = primitive.vertices
        for v1, v2 in zip(vertices, vertices[1:] + vertices[:1]):
            self._line(v1, v2)

    def _render_drill(self, primitive, color):
        self.log.debug('    drill(%r, %r)', primitive, color)

    def _render_slot(self, primitive, color):
        self.log.debug('    slot(%r, %r)', primitive, color)

    def _render_amgroup(self, primitive, color):
        self.log.debug('    amgroup(%r, %r)', primitive, color)

    def _render_test_record(self, primitive, color):
        self.log.debug('    test_record(%r, %r)', primitive, color)
