# Gerber Importer for Blender

This is an add-on for Blender 2.80+ to import Gerber files.

The add-on is not done yet, many of the possible shapes in Gerber files are not yet supported (for
example arcs and oblong round shapes).

Pull requests and issue reports can be filed at
[GitLab](https://gitlab.com/dr.sybren/import-gerber).


## PCB-Tools

This add-on bundles the [PCB-Tools](https://github.com/curtacircuitos/pcb-tools/) source code
in the `vendor/gerber` subdirectory.
